/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.calculadora;

/**
 *
 * @author extre
 */
public class Calculadora {

    private static int div(int v1, int v2){ 
        return v1/v2;
    }
    
    private static int sum(int v1, int v2){
        return v1+v2;
    }
    
    private static int sub(int v1, int v2){
        return v1-v2;
    }
    
    private static int mult(int v1, int v2){
        return v1*v2;
    }
    
    public static void main(String[] args) {
        //Métodos de casos de teste
        System.out.println("Divisão de 10/2 = "+div(10,2));
        System.out.println("Soma  de 10+2 = "+sum(10,2));
        System.out.println("Subtração  de 10-2 = "+sub(10,2));
        System.out.println("Multiplicação de 10*2 = "+mult(10,2));
    }
}
